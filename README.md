# k8s_argocd_skaffold


```sh
* docker rmi $(docker images -a -q) docker image prune docker volume prune
* docker login -u remotejob -p gitlabcom*** registry.gitlab.com
* go mod init

* go run ./service1

* curl localhost:8080/albums

* skaffold config set --global collect-metrics false
* skaffold config set --global local-cluster true
* skaffold config set --global default-repo remotejob

* minikube start
* minikube addons enable metrics-server (No need for now)
* minikube addons enable logviewer
* minikube service list
* eval $(minikube -p minikube docker-env) !!dont work om multi nodes
* skaffold dev -p development --no-prune=false --cache-artifacts=false  #--cleanup=false that won't perform the automatic cleanup.


* curl localhost:9001/albums

* kubectl kustomize enviroments/overlays/dev
* kubectl kustomize enviroments/overlays/prod
* kubectl kustomize enviroments/overlays/stag
* export KUBECONFIG=~/.kube_prod/config 

* argocd login argocd.cicd.ga

* kubectl config get-contexts -o name
* argocd cluster add default

* kubectl create ns gitops
* kubectl config set-context --current --namespace=gitops

** STAGING  skaffold config set --global default-repo registry.gitlab.com/remotejob/k8s_argocd_skaffold
** variant 1
* skaffold config set --global default-repo registry.gitlab.com/remotejob/k8s_argocd_skaffold
* kubectl create secret docker-registry gitlab-auth --docker-server=<your-registry-server> --docker-username=<your-name> --docker-password=<you email>
* skaffold build --push=true --file-output=tags.json
* skaffold render -p staging --build-artifacts tags.json -n gitops-staging -p staging --digest-source=local > deploy.yaml
* KUBECONFIG=~/.kube_prod/config skaffold -n gitops-staging apply deploy.yaml 

** variant 2 
* skaffold build --push=true  -q | KUBECONFIG=~/.kube_prod/config skaffold deploy -n gitops-staging -p staging --build-artifacts -

* KUBECONFIG=~/.kube_prod/config skaffold deploy -n gitops-staging -p staging --default-repo registry.gitlab.com/remotejob/k8s_argocd_skaffold --images service1:v0.22_linux_arm64

* KUBECONFIG=~/.kube_prod/config skaffold deploy -n gitops-staging -p staging --build-artifacts=tags.json

* VER=v0.17 skaffold build --push=true --default-repo remotejob
* KUBECONFIG=~/.kube_prod/config skaffold deploy -n gitops -p production --default-repo  --images service1:v0.17

* docker buildx use $(docker buildx create --platform linux/amd64,linux/arm64)

* docker buildx build -t remotejob/servicegitops1:v0.0 -f deployment/docker/service1.Dockerfile --platform linux/amd64,linux/arm64 --push service1/

* VER=v0.13 KUBECONFIG=~/.kube_prod/config skaffold run -p production --namespace=gitops --default-repo remotejob
* KUBECONFIG=~/.kube_prod/config skaffold deploy -p production --images service1:v0.14

* argocd app create k8s-argocd-skaffold --repo https://gitlab.com/remotejob/k8s_argocd_skaffold.git --path enviroments/overlays/prod --dest-server https://kubernetes.default.svc --dest-namespace gitops
* curl https://stag.edgecenter.ml/albums
* curl https://prod.edgecenter.ml/albums

```