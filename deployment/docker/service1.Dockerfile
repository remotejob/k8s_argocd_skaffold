FROM golang:1.19 as builder
RUN mkdir /build
WORKDIR /build
COPY .  .
RUN go mod download

ARG TARGETOS TARGETARCH
# RUN --mount=target=. \
#     --mount=type=cache,target=/root/.cache/go-build \
#     --mount=type=cache,target=/go/pkg \
#     GOOS=$TARGETOS GOARCH=$TARGETARCH CGO_ENABLED=0 go  build -a -o service1 .

RUN  GOOS=$TARGETOS GOARCH=$TARGETARCH CGO_ENABLED=0 go build -a -o service1 .   

FROM alpine
COPY --from=builder /build/service1 .
EXPOSE 8080
# Executable
ENTRYPOINT [ "./service1" ]


