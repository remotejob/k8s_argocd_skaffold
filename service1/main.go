package main

import (
	"fmt"
	"github.com/gin-gonic/gin"

	"net/http"
)

var (

	albums = []album{
		{ID: "1", Title: "Blue Train", Artist: "John Coltrane", Price: 56.99},
		{ID: "2", Title: "Jeru", Artist: "Gerry Mulligan", Price: 17.99},
		{ID: "3", Title: "Sarah Vaughan and Clifford Brown", Artist: "Sarah Vaughan1var2", Price: 49.99},
		{ID: "4", Title: "Sarah Vaughan and Clifford Brown", Artist: "Sarah Vaughan2", Price: 39.99},
		{ID: "5", Title: "Sarah Vaughan and Clifford Brown", Artist: "Sarah Vaughan3", Price: 39.99},
		{ID: "6", Title: "Sarah Vaughan and Clifford Brown", Artist: "Sarah Vaughan4", Price: 59.99},
	}
)

type album struct {
	ID     string  `json:"id"`
	Title  string  `json:"title"`
	Artist string  `json:"artist"`
	Price  float64 `json:"price"`
}

func main() {
	router := gin.New()
	router.GET("/albums", getAlbums)

	router.Run(":8080")
}

func getAlbums(c *gin.Context) {

	fmt.Println(c.Request.Header)

	c.IndentedJSON(http.StatusOK, albums)
}
